<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;

use App\Acme\Facades\Activity;
use App\Acme\Facades\General;
use Acme\Facades\Seo as SeoFacades;
use App\Page;
use App\PageContent;
use App\PageCategory;
use App\Seo;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($name = $request->name) {
            $data = Page::where('name', 'LIKE', '%' . $name . '%')->orderBy('order')->paginate(25);
        } else {
            $data = Page::orderBy('order')->paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        
        return view('admin/dev/pages/index')
            ->with('title', 'Pages')
            ->with('menu', 'dev-pages')
            ->with('keyword', $request->name)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/dev/pages/create')
            ->with('title', 'Create page')
            ->with('menu', 'pages')
            ->with('categories', PageCategory::pluck('name','id'));
    }
    
    public function store(PageRequest $request)
    {
        $input = $request->all();

        $duplicateName = Page::where("name", $input["name"])->first();
        $duplicateSlug = Page::where("slug", $input["slug"])->first();

        if($duplicateName) {
            $response = [
                'notifTitle'=>'Page name is alredy in use.',
                'notifMessage'=>'',
                'notifStatus'=>'error'
            ];
        } elseif($duplicateSlug) {
            $response = [
                'notifTitle'=>'Page slug is alredy in use.',
                'notifMessage'=>'',
                'notifStatus'=>'error'
            ];
        } else {
			if(isset($input['allow_add_contents'])) $input['allow_add_contents'] = 1;
			else $input['allow_add_contents'] = 0;

            if (!isset($input["published"])) {
                $input["published"] = "draft";
            } else {
                $input["published"] = "published";
            }
            
			$page = Page::create($input);

			$seoData['seoable_id'] = $page->id;
			SeoFacades::seoSave($seoData, $page);
			
			if ($page->allow_add_contents === 1) {

                $content = PageContent::where('slug','template-default')->first();
				if(!$content) $content = new PageContent;
				$content->slug = 'template-default';
				$content->enabled = 1;
				$content->editable = 1;
				$content->allow_add_items = 1;
				$content->order = 0;
				$content->page_id = $page->id;
				$content->save();
			}

            $response = [
                'notifTitle'=>'Save successful.',
                'notifMessage'=>'Redirecting to edit.',
                'notifStatus'=>'success',
                'redirect'=>route('adminPagesEdit', [$page->id])
            ];
        }

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/pages/show')
            ->with('title', 'Show page')
            ->with('data', Page::findOrFail($id));
    }

    public function view(Request $request, $slug)
    {
        $page = Page::with(['seo', 'clientContents.controls'])
                    ->where('slug', $slug);
        
        $page = $page->first();
        
        if ($page->allow_add_contents !== 1) {
            if (count($page->clientContents) == 1) {
				return view('admin/page_contents/edit')
					->with('menu', 'page-' . $page->slug)
                    ->with('title', $page->name)
                    ->with('menu', 'page-' . $slug)
                    ->with('cancelRoute', route('adminDashboard'))
                    ->with('page', $page)
                    ->with('seoData', $page->seo)
                    ->with('withSEO', true)
                    ->with('content', $page->clientContents[0])
                    ->with('data', $page->clientContents[0]);
            } else {
				return view('admin/page_contents/index')
					->with('menu', 'page-' . $page->slug)
                    ->with('title', $page->name)
                    ->with('seoData', $page->seo)
                    ->with('menu', 'page-' . $slug)
                    ->with('data', $page->clientContents)
                    ->with('page', $page);
            }
        } else {
			return view('admin/page_contents/index')
					->with('menu', 'page-' . $page->slug)
                    ->with('title', $page->name)
                    ->with('seoData', @$page->seo)
                    ->with('menu', 'page-' . $slug)
                    ->with('data', $page->clientContents)
                    ->with('page', $page);	
        }

    }
    // public function view($id)
    // {
    //     return view('admin/pages/view')
    //         ->with('title', 'View page')
    //         ->with('menu', 'pages')
    //         ->with('data', Page::findOrFail($id));
    // }
    public function order(Request $request)
    {
		
        $input=[];
		$data = $request->input('pages');
		// return $data;
        $newOrder=1;
        foreach($data as $d)
        {
            $input['order'] = $newOrder;
            $page_content = Page::findOrFail($d);
            $page_content->update($input);
            $newOrder++;
        }

         $response = [
            'notifTitle'=>'Order updated.',
        ];
        return response()->json($response);
    }

    public function edit($id)
    {
        $data = Page::with('pageContent.items')
                ->findOrFail($id);

		if ($data->allow_add_contents === 1) {
			$contentTemplate = PageContent::where('page_id', $id)->where('slug', 'template-default')->first();
			if ($contentTemplate) {
				$content = $contentTemplate;
			} else {
				$content = new PageContent;
				$content->slug = 'template-default';
				$content->enabled = 1;
				$content->editable = 1;
				$content->allow_add_items = 1;
				$content->order = 0;
				$content->page_id = $data->id;
				$content->save();
			}
		}
        
        return view('admin/dev/pages/edit')
            ->with('title', 'Edit Page')
            ->with('menu', 'dev-pages')
            ->with('content', @$content)
            ->with('data', $data);
    }

    public function update(PageRequest $request, $id)
    {
        $input = $request->all();
        // return $input;
        $duplicateName = Page::where("name", $input["name"])->where('id', '!=', $id)->first();
        $duplicateSlug = Page::where("slug", $input["slug"])->where('id', '!=', $id)->first();

        if($duplicateName) {
            $response = [
                'notifTitle'=>'Page name is alredy in use.',
                'notifMessage'=>'',
                'notifStatus'=>'error'
            ];
        } elseif($duplicateSlug) {
            $response = [
                'notifTitle'=>'Page slug is alredy in use.',
                'notifMessage'=>'',
                'notifStatus'=>'error'
            ];
        } else {
            $optionChanged = false;
            if (!isset($input["published"])) {
                $input["published"] = "draft";
            } else {
                $input["published"] = "published";
            }
			if(isset($input['allow_add_contents'])) $input['allow_add_contents'] = 1;
			else $input['allow_add_contents'] = 0;
			
            $page = Page::find($id);
			if ($input['allow_add_contents'] !== $page->allow_add_contents) $optionChanged = true;
			$page->update($input);
			if ($optionChanged) {
				if ($input['allow_add_contents'] === 1) {
					$content = new PageContent;
					$content->slug = 'template-default';
					$content->enabled = 1;
					$content->editable = 1;
					$content->allow_add_items = 1;
					$content->order = 0;
					$content->page_id = $page->id;
					foreach ($page->pageContent as $key => $contentAsOf) {
						if (count($contentAsOf->controls) > 0) $contentAsOf->controls()->delete();
					}
					if (count($page->pageContent) > 0) $page->pageContent()->delete();
					$content->save();
				} else {
                    $content = PageContent::where('slug','template-default')->where('page_id', $page->id)->first();
                    if(isset($content)){
                        $content->slug = '';
					    $content->save();
                    }
				}
			}

            $response = [
                'notifTitle'=>'Save successful.',
                'notifMessage'=>'Refreshing page.',
                'notifStatus'=>'success',
                'redirect'=>route('adminPagesEdit', [$page->id])
            ];
        }

        return response()->json($response);
        // $input = $request->all();
        // // return $input;
        // $page = Page::findOrFail($id);
        // $page->update($input);

        // // $log = 'edits a page "' . $page->name . '"';
        // // Activity::create($log);

        // $response = [
        //     'notifTitle'=>'Save successful.',
        // ];

        // return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = Page::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = Page::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->name;
        }
        // $log = 'deletes a new page "' . implode(', ', $names) . '"';
        // Activity::create($log);

        Page::destroy($input['ids']);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminPages')
        ];

        return response()->json($response);
    }
    
    public function pageToggle(Request $request) {
		$input = $request->all();
		
		$page = Page::findOrFail($input['id']);
		$page->published = $input['status'];
		$page->save();

		if ($page) {
            $response = [
                'success'=>true,
                'message'=>'Page status successfully changed.',
                'data'=>$page
            ];
        }
        else
        {
            $response = [
                'success'=>false,
                'message'=>'Failed to change review status.',
                'data'=>false
            ];
        }


        return response()->json($response);
	}
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/pages', array('as'=>'adminPages','uses'=>'Admin\PageController@index'));
Route::get('admin/pages/create', array('as'=>'adminPagesCreate','uses'=>'Admin\PageController@create'));
Route::post('admin/pages/', array('as'=>'adminPagesStore','uses'=>'Admin\PageController@store'));
Route::get('admin/pages/{id}/show', array('as'=>'adminPagesShow','uses'=>'Admin\PageController@show'));
Route::get('admin/pages/{id}/view', array('as'=>'adminPagesView','uses'=>'Admin\PageController@view'));
Route::get('admin/pages/{id}/edit', array('as'=>'adminPagesEdit','uses'=>'Admin\PageController@edit'));
Route::patch('admin/pages/{id}', array('as'=>'adminPagesUpdate','uses'=>'Admin\PageController@update'));
Route::post('admin/pages/seo', array('as'=>'adminPagesSeo','uses'=>'Admin\PageController@seo'));
Route::delete('admin/pages/destroy', array('as'=>'adminPagesDestroy','uses'=>'Admin\PageController@destroy'));
*/
}
