<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="range">Range</label>
  {!! Form::select('range', ['S' => 'Short', 'M' => 'Mid', 'L' => 'Long'], null, ['class'=>'form-control select2']) !!}
</div>
<div class="caboodle-form-group sumo-asset-select" data-crop-url="{{route('adminSamplesCropUrl')}}">
  <label for="image">Image</label>
  {!! Form::hidden('image', null, ['class'=>'sumo-asset', 'data-id'=>@$data->id, 'data-thumbnail'=>'image_thumbnail']) !!}
  <span class="sub-text-1">The required image size is 100x100 pixels minimum</span>
</div>
<div class="form-box sumo-asset-select-multi" data-id="{{ @$mainImages->id }}" data-name="SampleMainImages">
    <div class="header">
        Images
        {!! Form::hidden('main_images', json_encode(@$mainImages), ['class'=>'sumo-asset-multiple']) !!}
    </div>
    <span class="sub-text-1">The required image size is 100x100 pixels minimum</span>
</div>
<div class="form-box sumo-asset-select-multi" data-id="{{ @$secondaryImages->id }}" data-name="SampleSecondaryImages">
    <div class="header">
        Secondary Images
        {!! Form::hidden('secondary_images', json_encode(@$secondaryImages), ['class'=>'sumo-asset-multiple']) !!}
    </div>
    <span class="sub-text-1">The required image size is 100x100 pixels minimum</span>
</div>
<div class="caboodle-form-group">
  <label for="runes">Runes</label>
  {!! Form::text('runes', null, ['class'=>'form-control', 'id'=>'runes', 'placeholder'=>'Runes', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="embedded_rune">Embedded rune</label>
  {!! Form::text('embedded_rune', null, ['class'=>'form-control', 'id'=>'embedded_rune', 'placeholder'=>'Embedded rune', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="evaluation">Evaluation</label>
  {!! Form::textarea('evaluation', null, ['class'=>'form-control redactor', 'id'=>'evaluation', 'placeholder'=>'Evaluation', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>