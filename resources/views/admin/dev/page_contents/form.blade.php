
<input type="hidden" name="page_id" value="{{ $page->id }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="row">
    <div class="col-sm-8">
		@if($page->allow_add_contents !== 1)
    
        <div class="caboodle-card no-padding">
            <div class="caboodle-card-header no-padding margin-sides-20">
                <div class="pad-left-10 pad-right flex align-center">
                    <div class="animated-placeholder flex-1 {{@$data->name ? 'filled' : ''}}">
                        <input type="text" name="content_name" value="{{@$data->name}}" class="bundle-name-input" required>
                        <div class="placeholder">
                            <span class="add-on-empty">Enter c</span><span class="add-on-filled">C</span>ontent name <span class="add-on-empty">here...</span>
                        </div>
                    </div>
                    <div class="flex-auto text-center">
                        <div>
                            <label class="x-small" for="published">
                                Enabled
                            </label>
                        </div>
                        <div class="mdc-switch no-margin">
                            <input
                                type="checkbox"
                                id="basic-switch"
                                class="mdc-switch__native-control"
                                name="enabled"
                                {{ (@$data->enabled == 1 || !@$data )? 'checked' : '' }}
                            />
                            <div class="mdc-switch__background">
                                <div class="mdc-switch__knob"></div>
                            </div>
                        </div>
					</div>
                </div>
            </div>
            <div class="caboodle-card-body pad-left-30 pad-right-30">
                <div class="caboodle-form-group">
                    <label for="price">Slug</label>
                    {!! Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug', 'placeholder'=>'', 'autocomplete'=>'off', 'required']) !!}
				</div>
				<div class="caboodle-form-group">
					<label for="published">Published</label>
					{!! Form::select('published', ['draft' => 'draft', 'published' => 'published'], null, ['class'=>'form-control select2']) !!}
				</div>
				<div class="caboodle-form-group">
					<div>
						<label class="x-small" for="published">
							Editable
						</label>
					</div>
					<div class="mdc-switch no-margin">
						<input type="checkbox" id="basic-switch" class="mdc-switch__native-control" name="editable"
							{{ (@$data->editable == 1 || !@$data )? 'checked' : '' }} />
						<div class="mdc-switch__background">
							<div class="mdc-switch__knob"></div>
						</div>
					</div>
				</div>
				<div class="caboodle-form-group">
					<div>
						<label class="x-small" for="allow_add_items">
							Freely add items
						</label>
					</div>
					<div class="mdc-switch no-margin">
						<input type="checkbox" id="basic-switch" class="mdc-switch__native-control" name="allow_add_items"
						 {{ (@$data->allow_add_items == 1)? 'checked' : '' }} />
						<div class="mdc-switch__background">
							<div class="mdc-switch__knob"></div>
						</div>
					</div>
				</div>
            </div>
		</div>

		@endif
		@if(isset($data))
			@include('admin.dev.page_controls.card')
    	@endif
    
    </div>
    <div class="col-sm-4">
        @if(isset($data))
            <div class="caboodle-card">
                <div class="caboodle-card-header pad-top-15 pad-bottom-15">
                    <div class="filters no-padding">
                        <div class="flex align-center caboodle-form-control-connected">
                            <h4 class="no-margin flex-1">
								Content Items
							</h4>
							@if($data->allow_add_items !== 1)
                            <a
                                href="{{route('adminPageContentItemsCreate', $data->id)}}"
                                class="btn-transparent btn-sm flex-1 text-right">
                                <i class="far fa-plus-circle"></i> Add
							</a>
							@endif
                        </div>
                    </div>
                </div>
                <div class="caboodle-card-body">
					@if($data->allow_add_items !== 1)
                    	<a href="{{route('adminPageContentItemsCreate', $data->id)}}" class="{{$data->items->count() > 0 ? 'hide' : ''}}">
					@else
						<a href="{{route('adminPageContentItemsEdit', $item->id)}}">
					@endif
                        <div class="empty-message" role="button">
							@if($data->allow_add_items !== 1)
							Add a content items here...
							@else
							Set/Modify content items form controls here...
							@endif
                        </div>
					</a>
					<table width="100%">
						<tbody id="sortable" sortable-data-url="{{route('adminPageContentItemsOrder')}}">
							@if($data->allow_add_items !== 1)
								@foreach($data->items as $item)
								<tr sortable-id="page_content_items-{{$item->id}}">
									<td>
										<div class="flex align-center margin-bottom">
												<i style="color:#00a09a;" class="mr-3 fa fa-th-large sortable-icon" aria-hidden="true"></i><div class="flex-1 no-margin">
												@if ($item->name !== '' && @$item->name !== null)
												<strong>{{$item->name}}</strong>
												@else
												<span class="no-margin sub-text-2 uppercase">Unnamed content item</span>
												@endif
											</div>
											<div class="flex-auto">
												<a href="{{route('adminPageContentItemsEdit', $item->id)}}">
													<i
														class="far fa-edit hover-scale color-primary"
														role="button"
														data-toggle="tooltip"
														title="Edit"
													>
													</i>
												</a>
												&nbsp;
												<a href="{{ route('adminPageContentItemsDestroy', $item->id) }}" class="item-delete" data-id="{{ $item->id }}">
													<i class="far fa-trash hover-scale color-red" role="button" data-toggle="tooltip" title="Delete"></i>
												</a>
											</div>
										</div>
									</td>
								</tr>
								@endforeach
							@endif
						</tbody>
					</table>
                </div>
            </div>
        @endif
    </div>
</div>



@section('added-scripts')
@include('admin.pages.partials.added-script-ordering')
@include('admin.dev.page_controls.scripts')


<script>
   $('.item-delete').on('click', function(e) {
		e.preventDefault();
		var link = $(this).attr('href');
		var id = $(this).data('id');
		swal({
			title: "Delete item?",
			text: 'This action cannot be reversed after saving.',
			type: "warning",
			html: true,
			showCancelButton: true,
			confirmButtonColor: "#dc4f34",
			confirmButtonText: "Delete",
			cancelButtonText: "Cancel",
		}, function (isConfirm) {
			if (isConfirm) {
				showLoader(true);
				showNotifyToaster('info', '', '<i class="far fa-circle-notch fa-spin" style="margin-right: 5px"></i> Deleting item');
				$.ajax({
					type: "DELETE",
					url: link,
					data: { 
						_token: "{{ csrf_token() }}",
						id: id
					},
					success: function (data) {
						var title = data.notifTitle;
						var message = (_.isUndefined(data.notifMessage) ? '' : data.notifMessage);
						var status = (_.isUndefined(data.notifStatus) ? '' : data.notifStatus);

						if (status != '' && (title != '' || message != '')) {
							toastr.clear();
							showNotifyToaster(status, title, message);
						}
						setTimeout(function () {
							window.location.reload();
						}, 1500);
					},
					error: function (data, text, error) {
						showLoader(false);
						var message = '';
						_.each(data.responseJSON, function (val) {
							message += val + ' ';
						});
						toastr.clear();
						showNotifyToaster('error', 'Error deleting.', message);
					}
				});
			}
		});
	});

</script>

@endsection